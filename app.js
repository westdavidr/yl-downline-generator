require('async-arrays').proto();
var _ = require('underscore');
var Client = require('node-rest-client').Client;

var client = new Client();

client.registerMethod('randomUser', 'http://api.randomuser.me/', 'GET');

var ranks = {
  0: { name: 'Member', ogv: 0, legs: 0, leg_ogv: 0 },
  1: { name: 'Star', ogv: 500, legs: 0, leg_ogv: 0 },
  2: { name: 'Senior Star', ogv: 2000, legs: 0, leg_ogv: 0 },
  3: { name: 'Executive', ogv: 4000, legs: 2, leg_ogv: 1000 },
  4: { name: 'Silver', ogv: 10000, legs: 2, leg_ogv: 4000 },
  5: { name: 'Gold', ogv: 35000, legs: 3, leg_ogv: 6000 },
  6: { name: 'Platinum', ogv: 100000, legs: 4, leg_ogv: 8000 },
  7: { name: 'Diamond', ogv: 250000, legs: 5, leg_ogv: 15000 },
  8: { name: 'Crown Diamond', ogv: 750000, legs: 6, leg_ogv: 20000 },
  9: { name: 'Royal Crown Diamond', ogv: 1500000, legs: 6, leg_ogv: 35000 }
}

var rank = process.argv[4];

var pv = getPV(ranks[rank]);

getRandomUsers(pv.length, function(users) {
  var accounts = [];
  pv.forAllEmissions(function(item, index, done){
    getAccount(users[index].user, 1,  item, function(account) {
      accounts.push(account);
      done();
    });
  }, function(){
    //we're all done!
    var dl = {
      reportid: "",
      outfile: "",
      customerid: parseInt(process.argv[2]),
      periodid: parseInt(process.argv[3]),
      pagination: {
        totalrows: 80,
        totalpages: 1,
        startrow: 1,
        currentpage: 1,
        itemsperpage: 100
      },
      signature: "ABCD",
      accounts: accounts
    };
    console.log(dl);
  }); 
});

function getRandomUsers(count, cb) {
  var limit = 100;
  var users = [];
  
  var times = [];
  _(Math.ceil(count / limit)).times(function(n){
    times.push(n);
  });
  
  times.forAllEmissions(function(item, index, done) {
    client.methods.randomUser({parameters: {gender: 'female', results: count}}, function(data) {
      data = JSON.parse(data);
      data.results.forEach(function(e, i) {
        users.push(e);
      });
      done();
    });
  }, function(){
    cb(users);
  });
}

function getAccount(user, level, pv, callback) {
  callback({
    customerid: user.registered,
    name: user.name.last + ', ' + user.name.first,
    level: level,
    pv: pv,
    ogv: pv,
    pgv: pv,
    rankid: 1,
    activitystatusid: 1,
    customertype: 2,
    signupdate: randomDate(new Date(2012, 0, 1), new Date()),
    autoship: {
      active: false,
      day: 0,
      pv: 0,
      shipped: false
    },
    maincountryid: 1,
    avatarimage: "",
    onhold: false,
    holdreason: ""
  });
}

function getPV(rank) {
  var pvs = [];
  var max = rank.ogv;
  var sum = 0;
  
  if(rank.legs > 0) {
    _(rank.legs).times(function(n){
      pvs.push(rank.leg_ogv);
      sum += rank.leg_ogv;
    });
    max = rank.ogv -  _.reduce(pvs, function(memo, num){ return memo + num; }, 0);
  }
  while(sum < rank.ogv) {
    var pv = parseFloat(_.random(rank.ogv / 51, rank.ogv / _.random(25, 50)).toFixed(2));
    pv = pv > 500 ? 500 : pv;
    pvs.push(pv);
    sum += pv;
  }
  
  return pvs;
}

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}